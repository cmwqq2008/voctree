/**
 * File: Validator.h
 * Project: DVision library
 * Author: Amirreza Shaban
 * Date: December 1, 2014
 * Description: Validate if two images are matched
 */
 
#include <vector>
#include <cmath>
#include <opencv/cv.h>
#include "SurfValidator.h"
#include "DVision/SurfSet.h"

#include <DUtils/DUtils.h>
#include <DUtilsCV/DUtilsCV.h>



using namespace std;
using namespace DUtils;
using namespace DUtilsCV;
using namespace DVision;

// ----------------------------------------------------------------------------
    SurfValidator::~SurfValidator()
    {
        delete detector;
    }
// ----------------------------------------------------------------------------
    
    SurfValidator::SurfValidator(): ratio(.65)
    {
        detector = new cv::SurfFeatureDetector(2);
    }
// ----------------------------------------------------------------------------
    void SurfValidator::setImage1(cv::Mat &img1)
    {
        img1_keypoints.clear();
        img1_descriptors.release();
        #ifdef DEBUG_VALIDATOR
        timg1 = img1;
        #endif

        vector<cv::KeyPoint> keypoints;
        cv::Mat descriptors;

        detector->detect( img1, keypoints );
        extractor.compute(img1, keypoints, descriptors);
        setImage1(descriptors, keypoints);
    }

// ----------------------------------------------------------------------------
    void SurfValidator::setImage1(cv::Mat &descriptors, vector<cv::KeyPoint> &keypoints)
    {
        img1_descriptors = descriptors;
        img1_keypoints = keypoints;
    }
// ----------------------------------------------------------------------------
    float SurfValidator::validate(cv::Mat &img2, cv::Mat &H)
    {
        #ifdef DEBUG_VALIDATOR
        timg2 = img2;
        #endif
        /* this function is written based on opencv document's examlple */
         
        vector<cv::KeyPoint> img2_keypoints;
        cv::Mat img2_descriptors;
        detector->detect( img2, img2_keypoints );
        extractor.compute(img2, img2_keypoints, img2_descriptors);
        return validate(img2_descriptors, img2_keypoints, H);
    }
// ----------------------------------------------------------------------------
    float SurfValidator::validate(cv::Mat &img2_descriptors, vector<cv::KeyPoint> &img2_keypoints, cv::Mat &H)
    {
        cv::FlannBasedMatcher matcher;
        std::vector< std::vector<cv::DMatch> > matches;
        matcher.knnMatch(img1_descriptors,img2_descriptors, matches, 2);
        
        vector< cv::DMatch > good_matches;
        
        for (int i = 0; i < matches.size(); i++)
        {
            if (matches[i].size() > 1)
            {
                // check distance ratio
                if (matches[i][0].distance/
                    matches[i][1].distance < ratio)
                {
                    good_matches.push_back(matches[i][0]);
                }
            }
        }
        
        //cout << "Image1 key points: " << img1_keypoints.size() << " Image2 key points:" << img2_keypoints.size() << " Good matches: " << good_matches.size() << endl;
        
        if(good_matches.size() < 10)
        {
            return 0;
        }
        
        std::vector<cv::Point2f> img1_points;
        std::vector<cv::Point2f> img2_points;
        
        for( int i = 0; i < good_matches.size(); i++ )
        {
            img1_points.push_back( img1_keypoints[ good_matches[i].queryIdx ].pt );
            img2_points.push_back( img1_keypoints[ good_matches[i].trainIdx ].pt );
        }
        
        cv::Mat matches_mask;
        //cout << "img1_points: " << img1_points.size()<<endl;
        H = findHomography( img1_points, img2_points, CV_RANSAC, 5, matches_mask);
        
        int match_num = 0;
        vector< cv::DMatch > ransac_matches;
        for(int i = 0; i < matches_mask.rows; i++)
        {
            if((int)matches_mask.at<uchar>(i, 0) == 1)
            {
                match_num++;
                ransac_matches.push_back(good_matches[i]);
            }
            
        }
        //cout << "RANSAC Matches: " << match_num <<endl;
        //-- Get the corners from the image_1 ( the object to be "detected" )
        #ifdef DEBUG_VALIDATOR
        
        cv::Mat img_matches;
        drawMatches( timg1, img1_keypoints, timg2, img2_keypoints,
                    good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
                    vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
        
        std::vector<cv::Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( timg1.cols, 0 );
        obj_corners[2] = cvPoint( timg1.cols, timg1.rows ); obj_corners[3] = cvPoint( 0, timg1.rows );
        std::vector<cv::Point2f> scene_corners(4);
        
        perspectiveTransform( obj_corners, scene_corners, H);
        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        line( img_matches, scene_corners[0] + cv::Point2f( timg1.cols, 0), scene_corners[1] + cv::Point2f( timg1.cols, 0), cv::Scalar(0, 255, 0), 4 );
        line( img_matches, scene_corners[1] + cv::Point2f( timg1.cols, 0), scene_corners[2] + cv::Point2f( timg1.cols, 0), cv::Scalar( 0, 255, 0), 4 );
        line( img_matches, scene_corners[2] + cv::Point2f( timg1.cols, 0), scene_corners[3] + cv::Point2f( timg1.cols, 0), cv::Scalar( 0, 255, 0), 4 );
        line( img_matches, scene_corners[3] + cv::Point2f( timg1.cols, 0), scene_corners[0] + cv::Point2f( timg1.cols, 0), cv::Scalar( 0, 255, 0), 4 );
        
        //-- Show detected matches
        imshow( "Good Matches & Object detection", img_matches );
        cv::waitKey(0);
        #endif
        //cout << "RANSAC matches number: " << matches_mask.rows << endl;
        return sqrt((float)(good_matches.size()*good_matches.size())/(img1_keypoints.size() * img2_keypoints.size()));
        //perspectiveTransform( obj_corners, scene_corners, H);

    
    
    }
// ----------------------------------------------------------------------------



    float SurfValidator::validate2(cv::Mat &img2)
    {
        /*if(set1.empty())
        {
            return -1;
        }
        
        SurfSet set2;
        
        set2.Extract(img2, 400, false);
        
        
        vector<int> A_corr;
        vector<int> B_corr;
        bool remove_duplicates = true;
        double max_ratio = 0.8;
        set1.CalculateCorrespondences(set2,
                                 A_corr, B_corr,
                                 NULL,
                                 remove_duplicates,
                                 max_ratio);
        cout << A_corr.size() << ", " << B_corr.size() << endl;
        */
        
        
        return 0.0f;
    }
