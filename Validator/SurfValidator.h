/**
 * File: SurfValidator.h
 * Project: DVision library
 * Author: Amirreza Shaban
 * Date: December 1, 2014
 * Description: Validate if two images are matched
 */

#ifndef __SURF_VALIDATOR__
#define __SURF_VALIDATOR__

#include <opencv/cv.h>
#include <vector>
#include "DVision/SurfSet.h"

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/nonfree.hpp>

//#define DEBUG_VALIDATOR

#ifdef DEBUG_VALIDATOR
#include "opencv2/highgui/highgui.hpp"
#endif

/// Validate if two images are matched
class SurfValidator
{
public:

  /**
   * Creat a validator class object
   */
  SurfValidator();
    
  /**
   * Destructor
   */
  virtual ~SurfValidator();
    
  void setImage1(cv::Mat &img1);
  void setImage1(cv::Mat &img1_descriptors, std::vector<cv::KeyPoint> &img1_keypoints);
  float validate(cv::Mat &img2, cv::Mat &H);
  float validate2(cv::Mat &img2);
  float validate(cv::Mat &img2_descriptors, std::vector<cv::KeyPoint> &img2_keypoints, cv::Mat &H);
protected:
  DVision::SurfSet set1;
  cv::SurfFeatureDetector *detector = NULL;
  cv::SurfDescriptorExtractor extractor;
  std::vector<cv::KeyPoint> img1_keypoints;
  cv::Mat img1_descriptors;
  float ratio;
  #ifdef DEBUG_VALIDATOR
  cv::Mat timg1;
  cv::Mat timg2;
  #endif
};

#endif
