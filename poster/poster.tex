  \documentclass[final,t]{beamer}
\mode<presentation>
{
  \usetheme{I6dv}
}
% additional settings
\usepackage{graphicx}
\setbeamerfont{itemize}{size=\normalsize}
\setbeamerfont{itemize/enumerate body}{size=\normalsize}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\usepackage[ruled]{algorithm2e}
% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

\usefonttheme{structuresmallcapsserif} 

% additional packages
\usepackage{times}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{epstopdf}
\usepackage{Definitions}
\usepackage{multirow}
%\boldmath
\usepackage{booktabs, array}
%\usepackage{rotating} %sideways environment
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[orientation=landscape,size=custom,width=86.36,height=60.96,scale=1.3]{beamerposter}
\listfiles
\graphicspath{{fig/}}
% Display a grid to help align images
%\beamertemplategridbackground[1cm]

\title{ Online Loop Detection With Bag of Words: BRIEF vs. SIFT\vspace{1.5cm}
}
\author[Shaban et al.]{\large Amirreza Shaban 
\and Zhaoyang Lv
}
\institute{Georgia Institute of Technology}

%\date[Aug. 31 , 2007]{Aug. 31 , 2007}

% abbreviations
\usepackage{xspace}
\makeatletter
\DeclareRobustCommand\onedot{\futurelet\@let@token\@onedot}
\def\@onedot{\ifx\@let@token.\else.\null\fi\xspace}
\def\eg{{e.g}\onedot} \def\Eg{{E.g}\onedot}
\def\ie{{i.e}\onedot} \def\Ie{{I.e}\onedot}
\def\cf{{c.f}\onedot} \def\Cf{{C.f}\onedot}
\def\etc{{etc}\onedot}
\def\vs{{vs}\onedot}
\def\wrt{w.r.t\onedot}
\def\dof{d.o.f\onedot}
\def\etal{{et al}\onedot}
\makeatother
\newtheorem{keyfact}{Key Fact} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\TIL}{$\textsc{TCoverageLearner}$\xspace}
\newcommand{\CIC}{CIC\xspace}
\newcommand{\CICS}{CIC-S\xspace}
\newcommand{\DIC}{DIC\xspace}
\newcommand{\DICS}{DIC-S\xspace}
\newcommand{\NETRATE}{NETRATE\xspace}

\newcommand{\itemspaceing}{{\vspace{5mm}}}
\definecolor{brickred}{rgb}{0.5,0,0}
\begin{document}
\begin{frame}{} 
  
  
\begin{columns}[T]
  
%----------------------- the first column -------------------------------%
\begin{column}{.31\textwidth}

%----------------------- the new block -------------------------------%
\begin{block}{Abstract}
	\begin{itemize}        
		\item It is crucial for a robot to keep a consistent map of visited places efficiently.
		\item Loop detection should be fast and accurate.
		\item This process should take place online as robot moves and visit new places.
        \end{itemize}
\end{block}
	
%----------------------- the new block -------------------------------%
\begin{block}{Motivations} 
	\begin{columns}[T]
			\begin{column}{.6\textwidth} 	
			\begin{itemize}
				\item What's the efficient structure to manage features. \\[0.5cm]
				\item What's a good method for online training?
			\end{itemize}
			\end{column}
			\begin{column}{.38\textwidth} 	
				\begin{figure}[t]
					\includegraphics[width=1\textwidth]{./fig/loopclosure}
				\end{figure}
			\end{column}
		\end{columns}
		\vspace{.5cm}
		\begin{itemize}
			\item Feature descriptors matters for efficiency and accuracy 
		\end{itemize}
		\textcolor{brickred}{\bf\emph{ Can we solve these efficiently in real time ? }} \\[-0.5cm]
\end{block}
%----------------------- the new block -------------------------------%
{
\begin{block}{Algorithm Outline} 
		\begin{columns}[T]
			\begin{column}{.6\textwidth}
				\begin{itemize}
					\item For input image $I_i$: 
					    \begin{itemize}
					        \item Find BoW feature $v_i$ using vocabulary tree
					        \item Find set of potential similar Bow features $\mathcal{V}$ using inverse index
					        \item Calculate normalized scores images
					        \item Cutoff below a threshold
					    \end{itemize}
					\item Check temporal consistency
					\item Do geometrical validation
					 \begin{itemize}
					        \item Match descriptors using direct index
					        \item RANSAC
					\end{itemize}
				\end{itemize}
			\end{column}
			\begin{column}{.4\textwidth}
				\vspace{-1.6cm}
				\begin{figure}[t]
					\includegraphics[width=0.88\textwidth]{./fig/algorithm}
				\end{figure}
			\end{column}
		\end{columns}
\end{block}
}
\end{column}
 
%----------------------- the second column -------------------------------%
\begin{column}{.32\textwidth}

%----------------------- the new block -------------------------------%
\begin{block}{Descriptors and Data Structure}
\begin{columns}[T]
\begin{column}{0.5\textwidth}
\begin{itemize}
   \item BRIEF descriptors are fast to extract, SURF and SIFT are more accurate
   \item Direct index database for finding words in an image
\end{itemize}
\end{column}
\begin{column}{.5\textwidth}
				\begin{figure}[t]
					\includegraphics[width=\textwidth]{./fig/datastructure}
					\caption{[1]}
				\end{figure}
			\end{column}			
\end{columns}
\begin{itemize}
\item Inverse index database for finding images that have an specific word
\begin{itemize}
\item We compute the matching score for images that have at least one word in common with the query image
\end{itemize}
\item Vocabulary Tree:
 \begin{itemize}
 	\item Learning:
	\begin{itemize}
	\item The training image sequence is shrunk by sampling one frame from each time interval
	\item Descriptors are extracted from all the training images
	\item Vocabulary tree is learned is build over descriptors 
	\item Each layer of kd-tree is build by running k-means
         \item kd-tree is also learned with query images online
         \item idf for each word $w_i$: $idf(i) = \log \frac{N}{n_i}$
         \item inverse and direct index databases are updated accordingly
         \end{itemize}
         \item Testing:
         \begin{itemize}
         	         \item Term frequency each image: $tf(i, I_t) = \frac{n_{iI_t}}{n_{I_t}}$
	         \item $i^{th}$ entry of Bow feature: $v_t^i= tf(i, I_i) \times idf(i)$
	         \item L1 score function: $s(v_t, v_d) = 1 - \frac{1}{2} | \frac{v_1}{|v_1|} - \frac{v_2}{|v_2|} |$
	         \item Normalization: $\eta(v_t, v_d) = \frac{s(v_t, v_d)}{s(v_t, v_{t-1})}$
	\end{itemize}
         \end{itemize}
\end{itemize}
\end{block}

%----------------------- the new block -------------------------------%
\begin{block}{Validation}
	\begin{itemize}  
		\item Validation eliminates fasle positives: 
		\begin{itemize}
		\item For each descriptor 2-NN matches are found
		\item A match is accepted if $\frac{s(f, f_1)}{s(f, f_2)} > \rho$
		\item RANSAC is used to validate geometrical consistency of matches: 
		\begin{itemize}
		\item Fundamental matrix is computed in 2D matching
		\item Point cloud matching is used in 3D matching
		\end{itemize}
		\end{itemize} 
	\end{itemize}
%\vspace{-1cm}
\end{block}
\end{column}

%----------------------- the third column -------------------------------%

\begin{column}{.31\textwidth}

%----------------------- the new block -------------------------------%
\begin{block}{Evaluation}
\begin{tiny}
	\begin{itemize}
	         \item Use freiburg RGBD indoor dataset (More datasets are in the paper)
	         \item Try Different threshold values  $\lambda$ for filtering 
	         \item Time and precision comparison are provided for SIFT and BRIEF descriptors 
	\end{itemize}		
\end{tiny}
\end{block}

%----------------------- the new block -------------------------------%
\begin{block}{Evaluation : Results}
Figures explaination
    \begin{figure}[t]
 \centering
 \renewcommand{\tabcolsep}{2pt}
\begin{table}
\tiny
 \begin{tabular}{cc}
 		\begin{tabular}{cc}
 		\begin{tabular}{|c||c|c|c|c|}
			\hline 
   			 $\lambda$ & $0.65$ & $0.75$ & $0.85$ & $0.90$ \\
  			\hline 
			{SIFT} & 0.73(8/11)  & $0.73 (8/11)$ & $1 (5/5)$ & $1(2/2)$\\
  			\hline 
			{BRIEF} & INVALID & 0.27 (38/136) & 0.58 (49/84) & 0.77 (47/61)\\
			\hline 
		\end{tabular}&
		\begin{tabular}{|c|c|c|}
			\hline
   			 & Training Time (Sec) & Test Time (Sec) \\
			 \hline
			 {SIFT} & 1967 & 1.8 \\
			 \hline
  			{BRIEF} & 718 & 0.2 \\
			\hline
		\end{tabular}\\
		(a) Precision & (b) Time\\ 
		\end{tabular}\\
\includegraphics[width=.8\textwidth]{./fig/correct} \\
(c) True Match\\
\includegraphics[width=.8\textwidth]{./fig/wrong}\\
(d) Fals Positive\\
\includegraphics[width=.8\textwidth]{./fig/depth}\\
(e) Depth Image of False Positive\\
\end{tabular}
 \end{table}
\end{figure}
\vspace{-1cm}
\end{block}

\begin{block}{Conclusion}
\begin{itemize}
\item Conclusion 1
\item Conclusion 2
\item Conclusion 3
\end{itemize}
\end{block}

\end{column}




\end{columns}

\end{frame}

\end{document}
