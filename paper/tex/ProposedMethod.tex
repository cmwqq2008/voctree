\section{Proposed method}
\label{proposed_method}
In this section we explain the loop detection pipeline which is proposed in \cite{Galvez11iros} in more detail. To be able to do the deletion in real time BRIEF descriptor is used in the system. The pipeline has two major building parts: 1) Data base system \ref{fig:database}: It consists of the vocabulary tree to find the BoW representation of each image, inverse file system to find potential matches efficiently, and direct file which is used to efficiently match descriptors between two images. 2) Temporal and geometrical validation units. 
\begin{figure}[t]
\centering
\includegraphics[width=.5\textwidth]{./fig/datastructure}
\caption{Loop closure problem. Picture is taken from \cite{Galvez11iros}}
\label{fig:database}
\end{figure}
\subsection{BoW Database system}
For each image set of keypoints are extracted using FAST method \cite{rosten06eccv}. For each FAST keypoint a rectangular patch around it is used to extract the BRIEF descriptors \cite{calonder10eccv}. BRIEF descriptor extraction is very simple. For a given point ${\bf p}$ the i$^{th}$ bit of the descriptor vector $B_i({\bf p})$ is:
\begin{equation}
B_i({\bf p}) = 
\begin{dcases}
    1,& \text{if } {\bf p + x_i} \leq {\bf p + y_i}\\
    0,& \text{otherwise}
\end{dcases}
\end{equation}
in which ${\bf x_i}$ and ${\bf y_i}$ are offsets from the patch center ${\bf p}$ which are randomly selected in advance. Apparently, BRIEF descriptor is not invariant to rotation and scale. However, considering that BRIEF descriptors are very fast to compute and their discriminative power is high, they prove to be very efficient in loop detection problem.

\subsection{Database}
The vocabulary tree is built in the pre-training phase and also learns incrementally in an online manner. In the pre-training phase set of descriptors are extracted from the training images. Then they are clustered into $K$ cluster using k-means method. In the next tree level, we cluster descriptors in each of the clusters into $K$ clusters and do this process to build the tree with $L$ levels. At the end the tree has $K^L$ leaves which are the visual words in BoW method. Tree structure allows us to have a large number of clusters (up to 1 million) and still be able to compute BoW features very efficiently. Assuming that $n_i$ is the number of the occurrence of visual word $w_i$ in the training images and $N$ is total number of training images, weight of each visual word is its inverse document frequency \cite{Galvez11iros}:
\begin{equation}
idf(w_i) = \frac{N}{n_i}
\end{equation}
we use this weights to decrease effect of frequent visual word in the final feature vector. 

In the testing phase, for each descriptor in the query image $I_t$ the tree is traversed top to bottom and and in each level the node that has the minimum distance to the descriptor is selected. This computation allows us to calculate term frequency of each visual word in the image $I_t$:
\begin{equation}
tf(w_i, I_t) = \frac{n_{iI_t}}{n_{I_t}}
\end{equation}  
in which $n_{iI_t}$ is number of occurrence of visual word $w_i$ in $I_t$ and $n_{I_t}$ is the total number of descriptors in the image. Having term frequency of each visual word the i$^{th}$ element of BoW feature vector $v_t^i$ is calculated by normalizing it with the its inverse document frequency: $v_t^i =  tf(w_i, I_t) \times idf(w_i)$. Finally, the similarity between two BoW features are measured using L-1 score function:
\begin{equation}
score(v_1, v_2) = 1 - \frac{1}{2} | \frac{v_1}{|v_1|} - \frac{v_2}{|v_2|} |
\end{equation}

There are two other database elements that are used to improve time performance of the algorithm and are updated during the pre-training and online learning. First, an inverse index word structure that returns images that have a specific visual word in them. Therefor, for each query image we only need to compute the similarity score for the images that have at least one visual word in common with it. Second, a direct index which is used to store and find visual words in each image. Therefor, in the image validation part when we match BRIEF descriptors of two images, we only need to compare descriptors that belong to the same visual word not all the descriptors in the images. 

\subsection{Validation}   
As it is shown in figure \ref{fig:loop_closure_diagram} the output of database system is a set of images with highest similarity score. We use a filtering method to find the best match. Then temporal consistency of best match is validate with previous match. After this phase, the best match is validate with a geometrical consistency check using RANSAC \cite{fischler81acm} method. We also tried an extra phase of depth validation to reduce the false positive ratio of the system. The implementation details of these methods are given in the next section. 
