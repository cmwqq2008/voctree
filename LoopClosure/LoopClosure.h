/**
 * @file:   LoopClosure.h
 * @author: Zhaoyang Lv
 * @brief:  Loop Closure class for robot SLAM loop detection
 * @date:   Nov.29 2014
 */

#pragma once

#include <DBoW2/DBoW2.h>
#include <DBoW2/BowVector.h>

#include <DVision/BRIEF.h>

#include <DUtilsCV/DUtilsCV.h>

#if CV24
#include <opencv2/nonfree/features2d.hpp>
#endif

#include <vector>
#include <string>

/// use smart pointer
#include <boost/smart_ptr/shared_ptr.hpp>

/// image reader interface
#include <dataInterface/TUM3D.h>

class LoopClosure {

public:
  enum {SURF_TYPE, SIFT_TYPE, BRIEF_TYPE};

  typedef boost::dynamic_bitset<> bitset;
  typedef std::vector<std::vector<float> > VoCFloatingType;
  typedef std::vector<std::vector<std::vector<float> > > VoCFloatingVector;
  typedef std::vector<bitset > VoCBinaryType;
  typedef std::vector<std::vector<bitset > > VoCBinaryVector;

  /**
   * @brief The Configuration struct
   */
  struct Config{
    /**
     * the type feature used in this Loop Closure system
     */
    int feature_type_;
    /**
     * training image step, not all the images will be used for training
     */
    short training_image_step_;
    /**
     * the threshold for a query in image matching
     */
    double query_threshold_;
    /**
     * the threshold for a filtered query in image matching
     */
    double threshold_brief_, threshold_sift_, threshold_surf_;
    /**
     * ignore the number of neighbours in matching
     */
    short ignore_neighbourhood_num_;
    /**
     * branching factor and depth levels
     */
    int K_, L_;

    /**
     * weighting type and score type in metric
     */
    DBoW2::WeightingType weight_type_;
    DBoW2::ScoringType score_type_;

    /**
     * About the database
     */
    bool use_di;
    int di_levels;
  };

  Config config_;

  /**
   * constructor
   */
  LoopClosure(boost::shared_ptr<DataReader> reader,
              const int ssidx);

  /**
   * destructor
   */
  ~LoopClosure();

  /**
   * @brief init the parameters needed
   * @param feature descriptor type
   */
  void init(const int feature_type);

  /**
   * @brief train the Vocabulary trees with floating descriptors
   * @return a pointer to vector of float feature descriptor
   */
  boost::shared_ptr<VoCFloatingVector> trainVoC();

  /**
   * @brief train the Vocabulary trees with floating descriptors
   * @return a pointer to vector of binary feature descriptor
   */
  boost::shared_ptr<VoCBinaryVector> trainBinaryVoC();

  /**
   * @brief learn the image online
   * @param image idx of data source
   */
  void onlineLearning(const size_t image_idx);

  /**
   * @brief save the vocabulary tree
   * @param file name
   */
  void saveVoC(const std::string& file);

  /**
   * @brief load a saved vocabulary tree
   * @param file name
   */
  void loadVoC(const std::string& file);

  /**
   * query the database
   */
  bool queryDatabase(std::vector<std::vector<float> >& features,
                     DBoW2::QueryResults& query_results);

private:

  /**
   * @brief train surf features
   * @return pointer to the surf feature vectors
   */
  boost::shared_ptr<VoCFloatingVector> trainSurfVoC();

  /**
   * @brief train sift features
   * @return pointer to the sift feature vectors
   */
  boost::shared_ptr<VoCFloatingVector> trainSiftVoC();

  /**
   * @brief train brief features
   * @return pointer to the brief feature vectors
   */
  boost::shared_ptr<VoCBinaryVector> trainBriefVoC();

  /**
   * @brief online learning with SURF database
   * @param input gray-scale image
   */
  void onlineSURFLearning(cv::Mat& image, DBoW2::QueryResults& results);

  /**
   * @brief online learning with SIFT database
   * @param input gray-scale image
   */
  void onlineSIFTLearning(cv::Mat& image, DBoW2::QueryResults& results);

  /**
   * @brief online learning with BRIEF database
   * @param input gray-scale image
   */
  void onlineBRIEFLearning(cv::Mat& image, DBoW2::QueryResults& results);

  /**
   * @brief filter the query results and normalize the score w.r.t. the best score
   * @param the original query from database
   * @param filtered and normalized query
   * @param the matching score of two neighbours
   * @param threshold for valid matching
   */
  void filterAndNormalizeQuery(const DBoW2::QueryResults& query_src,
                               DBoW2::QueryResults& query_dst,
                               const double best_score,
                               const double threshold);

  void matchImages(const cv::Mat& image_src,
                   const cv::Mat& image_dst,
                   cv::Mat& pose);

  void depthValidation(const size_t img_index_src,
                       const size_t img_index_dst);

  /**
   * @brief change feature types to float type
   * @param openCV mat type features
   * @param float type features
   */
  void changeToFloat(const cv::Mat& plain, VoCFloatingType& out);

  /**
   * @brief print QueryResults
   * @param QueryResults
   */
  void printQueryResults(DBoW2::QueryResults& query);

  /**
   * the image reader interfaces
   */
  boost::shared_ptr<DataReader> data_reader_;
  /**
   * the image source index
   */
  size_t data_source_;
  /**
   * opencv feature detector
   */
  cv::Ptr<cv::FeatureDetector> detector_;
  /**
   * opencv feature extractor
   */
  cv::Ptr<cv::DescriptorExtractor> extractor_;
  /**
   * for specific Brief descriptor
   */
  boost::shared_ptr<DVision::BRIEF> brief;
  /**
   * shared pointer to Surf vocabulary tree
   */
  boost::shared_ptr<Surf64Vocabulary> surf_voc_ptr_;
  /**
   * shared pointer to Surf database
   */
  boost::shared_ptr<Surf64Database> surf_database_ptr_;
  /**
   * shared pointer to Surf vocabulary tree
   */
  boost::shared_ptr<SIFTVocabulary> sift_voc_ptr_;
  /**
   * shared pointer to Surf database
   */
  boost::shared_ptr<SIFTDatabase> sift_database_ptr_;
  /**
   * shared pointer to Brief vocabulary tree
   */
  boost::shared_ptr<BriefVocabulary> brief_voc_ptr_;
  /**
   * shared pointer to Brief database
   */
  boost::shared_ptr<BriefDatabase> brief_database_ptr_;
  /**
   * online frame number
   */
  size_t frame_num;
};
