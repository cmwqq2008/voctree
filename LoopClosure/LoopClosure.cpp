/**
 * @file:   LoopClosure.cpp
 * @author: Zhaoyang Lv
 * @brief:  Implementation of Loop Closure class for robot SLAM loop detection
 * @date:   Nov.29 2014
 */

#include "LoopClosure.h"
#include "LoopClosureHelper.h"

#include <DBoW2/BowVector.h>
#include <DUtils/DUtils.h>
#include <DVision/DVision.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/make_shared.hpp>


using namespace cv;
using namespace std;
using namespace boost;
using namespace DBoW2;
using namespace DVision;


/* ************************************************************************* */

LoopClosure::LoopClosure(shared_ptr<DataReader> reader,
                         const int ssidx)
  : data_reader_(reader),
    data_source_(ssidx){

  config_.feature_type_ = BRIEF_TYPE;
  config_.training_image_step_ = 16;
  config_.query_threshold_ = 0.1;
  config_.threshold_brief_ = 0.85;
  config_.threshold_sift_ = 0.7;
  config_.threshold_surf_ = 0.7;
  config_.ignore_neighbourhood_num_ = 16;

  config_.K_ = 64;
  config_.L_ = 4;

  config_.weight_type_ = TF_IDF;
  config_.score_type_ = L1_NORM;

  config_.use_di = false;
  config_.di_levels = 1;

  frame_num = 0;
}

/* ************************************************************************* */

LoopClosure::~LoopClosure() {

}

/* ************************************************************************* */
void LoopClosure::init(const int feature_type) {

  config_.feature_type_ = feature_type;

  cout << "FAST detector is used to detect all the features" << endl;

  detector_ = FeatureDetector::create("FAST");

  switch(feature_type) {
    case SURF_TYPE:
      extractor_ = DescriptorExtractor::create("SURF");
      break;
    case SIFT_TYPE:
      extractor_ = DescriptorExtractor::create("SIFT");
      break;
    case BRIEF_TYPE:
      brief = make_shared<BRIEF>(256, 48, BRIEF::RANDOM_CLOSE);
      break;
  }
}

/* ************************************************************************* */

shared_ptr<LoopClosure::VoCFloatingVector> LoopClosure::trainVoC() {

  switch(config_.feature_type_) {
    case SURF_TYPE:
      return trainSurfVoC();
    case SIFT_TYPE:
      return trainSiftVoC();
  }
}

/* ************************************************************************* */

shared_ptr<LoopClosure::VoCBinaryVector> LoopClosure::trainBinaryVoC() {
  // now we only work with brief
  return trainBriefVoC();
}

/* ************************************************************************* */

void LoopClosure::onlineLearning(const size_t image_idx) {

  frame_num = image_idx;

  Mat bgrImage, grayImage;
  data_reader_->readImage(data_source_, image_idx, bgrImage);
  cvtColor(bgrImage, grayImage, CV_BGR2GRAY);

  QueryResults query_results;
  switch(config_.feature_type_) {
    case SURF_TYPE:     // the two makes no different in learning now
      onlineSURFLearning(grayImage, query_results); break;
    case SIFT_TYPE:
      onlineSIFTLearning(grayImage, query_results); break;
    case BRIEF_TYPE:
      onlineBRIEFLearning(grayImage, query_results); break;
  }

  printQueryResults(query_results);

  if( !query_results.empty() ) {
    for(size_t i = 0; i < query_results.size(); i++) {

      size_t candidate_idx = query_results[i].Id;
      Mat candidate_bgr, candidate_gray;
      data_reader_->readImage(data_source_, candidate_idx, candidate_bgr);
      cvtColor(candidate_bgr, candidate_gray, CV_BGR2GRAY);

      Mat transformation;
      matchImages(candidate_gray, grayImage, transformation);
    }
  }

}

/* ************************************************************************* */

void LoopClosure::saveVoC(const string& file) {
  cout << "saving vocabularies to: " << file <<" ..."<< endl;
  switch(config_.feature_type_) {
    case SURF_TYPE:
      surf_voc_ptr_->save(file);
      break;
    case SIFT_TYPE:
      sift_voc_ptr_->save(file);
      break;
    case BRIEF_TYPE:
      brief_voc_ptr_->save(file);
      break;
  }
  cout << "Done!" << endl;
}

/* ************************************************************************* */

void LoopClosure::loadVoC(const string& file) {

  cout << "a database is created with " << file << "." << endl;

  switch(config_.feature_type_) {
    case SURF_TYPE:
      surf_voc_ptr_ = boost::make_shared<Surf64Vocabulary> (file);
      surf_database_ptr_ = boost::make_shared<Surf64Database> (*surf_voc_ptr_,
                                                               config_.use_di,
                                                               config_.di_levels);
      cout << "surf features loaded to the database..." << endl;
      break;
    case SIFT_TYPE:
      sift_voc_ptr_ = boost::make_shared<SIFTVocabulary> (file);
      sift_database_ptr_ = boost::make_shared<SIFTDatabase> (*sift_voc_ptr_,
                                                             config_.use_di,
                                                             config_.di_levels);
      cout << "sift features loaded to the database..." << endl;
      break;
    case BRIEF_TYPE:
      brief_voc_ptr_ = boost::make_shared<BriefVocabulary> (file);
      brief_database_ptr_ = boost::make_shared<BriefDatabase> (*brief_voc_ptr_,
                                                               config_.use_di,
                                                               config_.di_levels);
      cout << "brief features loaded to the database..." << endl;
      break;
  }

}



/* ************************************************************************* */

shared_ptr<LoopClosure::VoCFloatingVector> LoopClosure::trainSurfVoC() {

  size_t image_num = data_reader_->numRgbImages() / config_.training_image_step_;
  shared_ptr<VoCFloatingVector> features(new VoCFloatingVector );
  features->reserve(image_num);

  cout << image_num << " will be used for training" << endl;

  for(size_t i = 0; i < image_num; i++) {

    Mat rgbImage, grayImage;
    size_t image_idx = i * config_.training_image_step_;
    data_reader_->readImage(data_source_, image_idx, rgbImage);
    cvtColor(rgbImage, grayImage, CV_BGR2GRAY);

    vector<KeyPoint> keypoints;
    detector_->detect(grayImage, keypoints);
    Mat descriptors;
    extractor_->compute(grayImage, keypoints, descriptors);

    features->push_back(VoCFloatingType() );
    changeToFloat(descriptors, features->back());
  }

  cout << "create Vobabulary tree with: " <<  config_.K_ << "^" << config_.L_ << endl;

  surf_voc_ptr_ = make_shared<Surf64Vocabulary> (config_.K_, config_.L_,
                                                 config_.weight_type_,
                                                 config_.score_type_);
  surf_voc_ptr_->create(*features);
  cout << "a vocabulary tree is created! Info:" << endl
       << *surf_voc_ptr_ << endl;

  surf_database_ptr_ = make_shared<Surf64Database> (*surf_voc_ptr_,
                                                    config_.use_di,
                                                    config_.di_levels);
  cout << "a vocabulatry tree data base is created!" << endl;

  return features;
}

/* ************************************************************************* */

shared_ptr<LoopClosure::VoCFloatingVector> LoopClosure::trainSiftVoC() {

  size_t image_num = data_reader_->numRgbImages() / config_.training_image_step_;
  shared_ptr<VoCFloatingVector> features(new VoCFloatingVector );
  features->reserve(image_num);

  cout << image_num << " will be used for training" << endl;

  for(size_t i = 0; i < image_num; i++) {

    Mat rgbImage, grayImage;
    size_t image_idx = i * config_.training_image_step_;
    data_reader_->readImage(data_source_, image_idx, rgbImage);
    cvtColor(rgbImage, grayImage, CV_BGR2GRAY);

    vector<KeyPoint> keypoints;
    detector_->detect(grayImage, keypoints);
    Mat descriptors;
    extractor_->compute(grayImage, keypoints, descriptors);

    features->push_back(VoCFloatingType() );
    changeToFloat(descriptors, features->back());
  }

  cout << "create Vobabulary tree with: " <<  config_.K_ << "^" << config_.L_ << endl;

  sift_voc_ptr_ = make_shared<SIFTVocabulary> (config_.K_, config_.L_,
                                               config_.weight_type_,
                                               config_.score_type_);
  sift_voc_ptr_->create(*features);
  cout << "a vocabulary tree is created! Info:" << endl
       << *sift_voc_ptr_ << endl;

  sift_database_ptr_ = make_shared<SIFTDatabase> (*sift_voc_ptr_,
                                                  config_.use_di, config_.di_levels);
  cout << "a vocabulatry tree data base is created!" << endl;

  return features;
}

/* ************************************************************************* */

shared_ptr<LoopClosure::VoCBinaryVector> LoopClosure::trainBriefVoC() {

  size_t image_num = data_reader_->numRgbImages() / config_.training_image_step_;
  shared_ptr<VoCBinaryVector> features(new VoCBinaryVector );
  features->reserve(image_num);

  cout << image_num << " will be used for training" << endl;

  for(size_t i = 0; i < image_num; i++) {

    Mat rgbImage, grayImage;
    size_t image_idx = i * config_.training_image_step_;
    data_reader_->readImage(data_source_, image_idx, rgbImage);
    cvtColor(rgbImage, grayImage, CV_BGR2GRAY);

    vector<KeyPoint> keypoints;
    detector_->detect(grayImage, keypoints);
    features->push_back(VoCBinaryType() );
    brief->compute(grayImage, keypoints, features->back(), true);
  }

  cout << "create Vobabulary tree with: " <<  config_.K_ << "^" << config_.L_ << endl;

  brief_voc_ptr_ = make_shared<BriefVocabulary> (config_.K_, config_.L_,
                                                 config_.weight_type_,
                                                 config_.score_type_);
  brief_voc_ptr_->create(*features);
  cout << "a vocabulary tree is created! Info:" << endl
       << *brief_voc_ptr_ << endl;

  brief_database_ptr_ = make_shared<BriefDatabase> (*brief_voc_ptr_,
                                                    config_.use_di,
                                                    config_.di_levels);
  cout << "a vocabulatry tree data base is created!" << endl;

  return features;
}

/* ************************************************************************* */

void LoopClosure::onlineSURFLearning(cv::Mat& image, QueryResults& results) {
  vector<KeyPoint> keypoints;
  detector_->detect(image, keypoints);

  Mat descriptors;
  extractor_->compute(image, keypoints, descriptors);
  VoCFloatingType features;
  changeToFloat(descriptors, features);

  QueryResults query_results;
  int query_results_num(6);
  surf_database_ptr_->query(features, query_results, query_results_num);

  printQueryResults(query_results);

  static VoCFloatingType last_surf_features;

  if(frame_num > 10 && !query_results.empty() &&
     query_results.front().Score > config_.query_threshold_) {

    BowVector v1, v2;
    surf_voc_ptr_->transform(last_surf_features, v1);
    surf_voc_ptr_->transform(features, v2);
    double neighbour_score = surf_voc_ptr_->score(v1, v2);

    filterAndNormalizeQuery(query_results, results, neighbour_score, config_.threshold_surf_);

  }

  surf_database_ptr_->add(features);

  last_surf_features = features;

}

/* ************************************************************************* */

void LoopClosure::onlineSIFTLearning(cv::Mat& image, QueryResults& results) {
  vector<KeyPoint> keypoints;
  detector_->detect(image, keypoints);

  Mat descriptors;
  extractor_->compute(image, keypoints, descriptors);
  VoCFloatingType features;
  changeToFloat(descriptors, features);

  QueryResults query_results;
  int query_results_num(6);
  sift_database_ptr_->query(features, query_results, query_results_num);

  static VoCFloatingType last_sift_features;

  if(frame_num > 10 && !query_results.empty() &&
     query_results.front().Score > config_.query_threshold_) {

    BowVector v1, v2;
    sift_voc_ptr_->transform(last_sift_features, v1);
    sift_voc_ptr_->transform(features, v2);
    double neighbour_score = sift_voc_ptr_->score(v1, v2);

    filterAndNormalizeQuery(query_results, results, neighbour_score, config_.threshold_sift_);
  }

  sift_database_ptr_->add(features);

  last_sift_features = features;

}

void LoopClosure::onlineBRIEFLearning(cv::Mat& image, QueryResults& results) {

  vector<KeyPoint> keypoints;
  detector_->detect(image, keypoints);

  VoCBinaryType features;
  brief->compute(image, keypoints, features, true);

  QueryResults query_results;
  int query_results_num(10);
  brief_database_ptr_->query(features, query_results, query_results_num);

  static VoCBinaryType last_brief_features;

  if(frame_num > 10 && !query_results.empty() &&
     query_results.front().Score > config_.query_threshold_) {

    BowVector v1, v2;
    brief_voc_ptr_->transform(last_brief_features, v1);
    brief_voc_ptr_->transform(features, v2);
    double neighbour_score = brief_voc_ptr_->score(v1, v2);

    filterAndNormalizeQuery(query_results, results, neighbour_score, config_.threshold_brief_);

  }

  brief_database_ptr_->add(features);

  last_brief_features = features;

}

/* ************************************************************************* */

void LoopClosure::filterAndNormalizeQuery(const QueryResults &query_src,
                                          QueryResults &query_dst,
                                          const double best_score,
                                          const double threshold) {
  size_t query_num = query_src.size();

  for(size_t i = 0; i < query_num; i++) {

    if(abs(frame_num - query_src[i].Id) < config_.ignore_neighbourhood_num_) {
      continue;       // ignore the adjecent matches
    }

    double new_score = query_src[i].Score / best_score;
    if(new_score < threshold) {
      break;     // The query_src is ordered reversely. No need to do further check
    } else {
      query_dst.push_back(query_src[i]);
      query_dst.back().Score = new_score;
    }
  }
}

void LoopClosure::matchImages(const Mat& image_src, const Mat& image_dst, Mat& pose) {

  vector<KeyPoint> keypoints_src, keypoints_dst;

  SurfDescriptorExtractor surf_extractor;     // we will use this extractor for all the image matching

  Mat descriptors_src, descriptors_dst;
  surf_extractor.compute(image_src, keypoints_src, descriptors_src);
  surf_extractor.compute(image_dst, keypoints_dst, descriptors_dst);

  FlannBasedMatcher matcher;
  vector<DMatch> matches;
  matcher.match(descriptors_src, descriptors_dst, matches);

  double min_dst = 100;
  for(int i=0; i<descriptors_src.rows; i++) {
    double dist = matches[i].distance;
    if(dist < min_dst) {
      min_dst = dist;
    }
  }

  vector<DMatch> filtered_match;
  for(int i=0; i < descriptors_src.rows; i++) {
    if(matches[i].distance < 3 * min_dst) {
      filtered_match.push_back(matches[i]);
    }
  }

  Mat matched_image;
  drawMatches(image_src, keypoints_src, image_dst, keypoints_dst, filtered_match,
              matched_image);

  vector<Point2f> src_2d;
  vector<Point2f> dst_2d;

  for(int i=0; i < filtered_match.size(); i++) {
    src_2d.push_back(keypoints_src[filtered_match[i].queryIdx ].pt );
    dst_2d.push_back(keypoints_dst[filtered_match[i].trainIdx ].pt );
  }

//  Mat H = findHomography(src_2d, dst_2d, CV_RANSAC, 2);

  imshow("Matches", matched_image);
  waitKey(0);

}

void LoopClosure::depthValidation(const size_t img_index_src,
                                  const size_t img_index_dst) {

  Mat depthImage_src, depthImage_dst;
  data_reader_->readDepth(data_source_, img_index_src, depthImage_src);
  data_reader_->readDepth(data_source_, img_index_dst, depthImage_dst);

//  pcl::PointCloud<pcl::PointXYZ> point_cloud_src, point_cloud_dst;

//  gtsam::Cal3_S2::shared_ptr camera_intr = data_reader_->cameraIntrinsic(data_source_);

//  createPointCloudFromDepthImage(depthImage_src, point_cloud_src, camera_intr);
//  createPointCloudFromDepthImage(depthImage_dst, point_cloud_dst, camera_intr);
}

/* ************************************************************************* */

void LoopClosure::changeToFloat(const Mat &plain, VoCFloatingType&out)
{
  int L = plain.cols;
  out.resize(plain.rows);

  for(int i = 0; i < plain.rows; i++)
  {
    out[i].resize(L);
    plain.row(i).copyTo(out[i]);
  }
}

void LoopClosure::printQueryResults(QueryResults& query) {

  cout << "current image num is: " << frame_num << "\t";

  if( query.empty() ) {
    cout << "no single relevant match found." << endl;
  } else if (query.front().Score < config_.query_threshold_ ) {
    cout << "no salient matches found." << endl;
  } else {
    cout << "potential good matches found:" << query << endl;
  }

}

