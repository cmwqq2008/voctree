///**
// * @file:   LoopClosureHelper.h
// * @author: Zhaoyang Lv
// * @brief:  Loop Closure Helper functions for robot SLAM loop detection
// * @date:   Dec.3 2014
// */

//#pragma once

//#include <pcl/point_types.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/impl/point_types.hpp>

//#include <opencv2/core/mat.hpp>
//#include <opencv2/core/core.hpp>

//#include <gtsam/geometry/Cal3_S2.h>

//void createPointCloudFromDepthImage(const cv::Mat& depthImage,
//                                    pcl::PointCloud<pcl::PointXYZ>& point_cloud,
//                                    gtsam::Cal3_S2::shared_ptr camera_intr);

