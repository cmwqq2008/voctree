file(GLOB tests "*.cpp")

foreach(test ${tests})
  get_filename_component(target ${test} NAME_WE)
  add_executable(${target} ${DBoW} ${DUtils} ${DUtilsCV} ${DVision} ${LoopClosure})
  target_link_libraries(${target}
    ${OpenCV_LIBS}
    ${BOOST_LIBS}
    ${PCL_LIBRARIES}
    dataInterface-shared
    gtsam)
endforeach(test)
