/**
 * File: testSIFTDemo.cpp
 * Date: November 2011
 * Author: Zhaoyang Lv
 * Description: demo application of BRIEF features based on DBoW2
 */

#include <iostream>
#include <vector>

// DBoW2
#include <DBoW2/DBoW2.h> // defines Surf64Vocabulary and Surf64Database

#include <DUtils/DUtils.h>
#include <DUtilsCV/DUtilsCV.h> // defines macros CVXX
#include <DVision/DVision.h>
#include <DVision/BRIEF.h>

// OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#if CV24
#include <opencv2/nonfree/features2d.hpp>
#endif


using namespace DBoW2;
using namespace DUtils;
using namespace DVision;
using namespace std;
using namespace cv;

typedef boost::dynamic_bitset<> bitset;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void loadBRIEFFeatures(vector<vector<bitset> >& features);
void changeStructure(const Mat &plain, vector<bitset>& out);
void changeStructure(const vector<float> &plain, vector<vector<float> > &out, int L);
void testVocCreation(const vector<vector<bitset > > &features);
void testDatabase(const vector<vector<bitset > >& features);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// number of training images
const int NIMAGES = 4;

// extended surf gives 128-dimensional vectors
const bool EXTENDED_SURF = false;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void wait()
{
  cout << endl << "Press enter to continue" << endl;
  getchar();
}

// ----------------------------------------------------------------------------

int main()
{
  vector<vector<bitset > > features;
  loadBRIEFFeatures(features);

  testVocCreation(features);

  wait();

  testDatabase(features);

  return 0;
}

// ----------------------------------------------------------------------------

void loadBRIEFFeatures(vector<vector<bitset > > &features)
{
  features.clear();
  features.reserve(NIMAGES);

  // use a general Feature Detector to detect features in the image
  Ptr<FeatureDetector> detector = FeatureDetector::create("FAST");
//  Ptr<DescriptorExtractor> descriptorExtractor = DescriptorExtractor::create("BRIEF");

  BRIEF brief(256, 48, BRIEF::RANDOM_CLOSE);

  cout << "Extracting SURF features..." << endl;
  for(int i = 0; i < NIMAGES; ++i)
  {
    stringstream ss;
    ss << "../../images/image" << i << ".png";

    Mat image = imread(ss.str(), 0);
    vector<KeyPoint> keypoints;
    detector->detect(image, keypoints);
    features.push_back(vector<bitset >());
    brief.compute(image, keypoints, features.back(), true);
  }
}

// ----------------------------------------------------------------------------

void changeStructure(const Mat &plain, vector<bitset > &out)
{
  int L = plain.cols;
  out.resize(plain.rows);
}


void changeStructure(const vector<float> &plain, vector<vector<float> > &out, int L)
{
  out.resize(plain.size() / L);

  unsigned int j = 0;
  for(unsigned int i = 0; i < plain.size(); i += L, ++j)
  {
    out[j].resize(L);
    std::copy(plain.begin() + i, plain.begin() + i + L, out[j].begin());
  }
}

// ----------------------------------------------------------------------------

void testVocCreation(const vector<vector<bitset > >& features)
{
  // branching factor and depth levels
  const int k = 9;
  const int L = 3;
  const WeightingType weight = TF_IDF;
  const ScoringType score = L1_NORM;

  BriefVocabulary voc(k, L, weight, score);

  cout << "Creating a small " << k << "^" << L << " vocabulary..." << endl;
  voc.create(features);
  cout << "... done!" << endl;

  cout << "Vocabulary information: " << endl
  << voc << endl << endl;

  // lets do something with this vocabulary
  cout << "Matching images against themselves (0 low, 1 high): " << endl;
  BowVector v1, v2;
  for(int i = 0; i < NIMAGES; i++)
  {
    voc.transform(features[i], v1);
    for(int j = 0; j < NIMAGES; j++)
    {
      voc.transform(features[j], v2);

      double score = voc.score(v1, v2);
      cout << "Image " << i << " vs Image " << j << ": " << score << endl;
    }
  }

  // save the vocabulary to disk
  cout << endl << "Saving vocabulary..." << endl;
  voc.save("small_voc.yml.gz");
  cout << "Done" << endl;
}

// ----------------------------------------------------------------------------

void testDatabase(const vector<vector<bitset > > &features)
{
  cout << "Creating a small database..." << endl;

  // load the vocabulary from disk
  BriefVocabulary voc("small_voc.yml.gz");

  BriefDatabase db(voc, false, 1); // false = do not use direct index
  // (so ignore the last param)
  // The direct index is useful if we want to retrieve the features that
  // belong to some vocabulary node.
  // db creates a copy of the vocabulary, we may get rid of "voc" now

  // add images to the database
  for(int i = 0; i < NIMAGES; i++)
  {
    db.add(features[i]);
  }

  cout << "... done!" << endl;

  cout << "Database information: " << endl << db << endl;

  // and query the database
  cout << "Querying the database: " << endl;

  QueryResults ret;
  for(int i = 0; i < NIMAGES; i++)
  {
    db.query(features[i], ret, 4);

    // ret[0] is always the same image in this case, because we added it to the
    // database. ret[1] is the second best match.

    cout << "Searching for Image " << i << ". " << ret << endl;
  }

  cout << endl;

  // we can save the database. The created file includes the vocabulary
  // and the entries added
  cout << "Saving database..." << endl;
  db.save("small_db.yml.gz");
  cout << "... done!" << endl;

  // once saved, we can load it again
  cout << "Retrieving database once again..." << endl;
  BriefDatabase db2("small_db.yml.gz");
  cout << "... done! This is: " << endl << db2 << endl;
}

// ----------------------------------------------------------------------------


