/**
 * @file:   testTrainingDBoW.cpp
 * @author: Zhaoyang Lv
 * @brief:  test file for training the Bag of Words system
 * @date:   Nov.30 2014
 */

#include <LoopClosure/LoopClosure.h>

#include <DBoW2/QueryResults.h>

#include <dataInterface/TUM3D.h>

#include <gtsam/base/timing.h>

using namespace cv;
using namespace std;
using namespace boost;
using namespace DBoW2;


int main(int argc, char* argv[]) {

  shared_ptr<Tum3D> tum3d(new Tum3D);

  const size_t source_index(101);    //rgbd_dataset_freiburg1_360
  gttic_(Parse_Input_Files);
  tum3d->parseSequenceFile(source_index);
  gttoc_(Parse_Input_Files);


  std::string use_feature("BRIEF");
  std::string voc_file;

  LoopClosure loop_closure(tum3d, source_index);

  if(argc > 1) {
    use_feature = argv[1];
  }

  if( use_feature == "BRIEF" ) {

    loop_closure.init(LoopClosure::BRIEF_TYPE);

    gttic_(Train_Brief_VoC);
    shared_ptr<LoopClosure::VoCBinaryVector> b_features = loop_closure.trainBinaryVoC();
    gttoc_(Train_Brief_VoC);

    voc_file = "../results/brief_voc.yml.gz";

  } else if( use_feature == "SIFT" ) {

    loop_closure.init(LoopClosure::SIFT_TYPE);

    gttic_(Train_SIFT_VoC);
    shared_ptr<LoopClosure::VoCFloatingVector> f_features = loop_closure.trainVoC();
    gttoc_(Train_SIFT_VoC);

    voc_file = "../results/sift_voc.yml.gz";

  } else if ( use_feature == "SURF" ) {

    loop_closure.init(LoopClosure::SURF_TYPE);

    gttic_(Train_SURF_VoC);
    shared_ptr<LoopClosure::VoCFloatingVector> f_features = loop_closure.trainVoC();
    gttoc_(Train_SURF_VoC);

    voc_file = "../results/surf_voc.yml.gz";

  }

  gttic_(Save_VoC);
  loop_closure.saveVoC(voc_file);
  gttoc_(Save_VoC);

//  size_t total_image =  tum3d->numRgbImages();
  size_t total_image = 40;

  // check every two frame images
  gttic_(Online_Learn_Features);
  for(size_t i =0; i < total_image; i += 3) {
    loop_closure.onlineLearning(i);
  }
  gttoc_(Online_Learn_Features);

  return 0;
}
