/**
 * File: Demo.cpp
 * Date: November 2011
 * Author: Dorian Galvez-Lopez
 * Description: demo application of DBoW2
 */

#include <iostream>
#include <vector>

// DBoW2
#include <DBoW2/DBoW2.h> // defines SIFTVocabulary and SIFTDatabase

#include <DUtils/DUtils.h>
#include <DUtilsCV/DUtilsCV.h> // defines macros CVXX
#include <DVision/DVision.h>
#include <Validator/SurfValidator.h>
// OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#if CV24
#include <opencv2/nonfree/features2d.hpp>
#endif

#include <typeinfo>  //for 'typeid'

using namespace DBoW2;
using namespace DUtils;
using namespace std;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void wait()
{
  cout << endl << "Press enter to continue" << endl;
  getchar();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// number of training images
const int NIMAGES = 4;

// ----------------------------------------------------------------------------

int main()
{
    SurfValidator validator;
    cv::Mat H;
    for(int i = 0; i < NIMAGES; ++i)
    {
        stringstream ss1;
        ss1 << "../images/image" << i << ".png";
        cv::Mat im1 = cv::imread(ss1.str(), CV_LOAD_IMAGE_GRAYSCALE);
        validator.setImage1(im1);
        for(int j = i; j < NIMAGES; ++j)
        {
            stringstream ss2;
            ss2 << "../images/image" << j << ".png";
            cv::Mat im2 = cv::imread(ss2.str(), CV_LOAD_IMAGE_GRAYSCALE);
            float sim = validator.validate(im2, H);
            cout << "Image " << i << " vs Image " << j << ": " <<  sim << endl;
        }
    }
    
  return 0;
}

// ----------------------------------------------------------------------------


